# Modbus RTU over Moxa Nport 5610

An modbus server / slave will be simulated using the python package pymodbus on
the "real" com port `/dev/ttyr0f`. The EPICS IOC will connect to an TCP IP port
on the Moxa.

1.  Setup Nport

    * Port 15 TCP Server mode
    * Port 16 Real Com mode

2.  Run modbus server
    
```
python server.py
```

3.  Run EPICS IOC

```
make -C modules/test
iocsh st.cmd
```


It is now possible to check if the modbus connection is working by writing to `coil1:w` and monitoring `coil1:r`
