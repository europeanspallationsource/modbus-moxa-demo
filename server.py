#!/usr/bin/python

from pymodbus.server.sync import ModbusSerialServer
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext

from pymodbus.transaction import ModbusRtuFramer, ModbusAsciiFramer

import logging
logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.DEBUG)

identity = ModbusDeviceIdentification()
store = ModbusSlaveContext(co = ModbusSequentialDataBlock(0, [0]*100), hr = ModbusSequentialDataBlock(0, [0]*100))
context = ModbusServerContext(slaves=store, single=True)

#StartSerialServer(context, framer=,)

server = ModbusSerialServer(context, framer=ModbusRtuFramer, identity=identity, port='/dev/ttyr0f', baudrate=115200, timeout=0.1)
#server = ModbusSerialServer(context, framer=ModbusAsciiFramer, identity=identity, port='/dev/ttyr0f', baudrate=115200)
server.serve_forever()
