require modbus
require test
 
#Specify the TCP endpoint and give your bus a name eg. "tcpcon1".
drvAsynIPPortConfigure("tcpcon1","10.4.3.90:4015",0,0,1)


#Enable terminators in ascii
#asynOctetSetOutputEos("tcpcon1",0,"\r\n")
#asynOctetSetInputEos("tcpcon1",0,"\r\n")

#Configure the interpose for RTU
modbusInterposeConfig("tcpcon1", 1, 0, 0)
#Configure the interpose for Ascii
#modbusInterposeConfig("tcpcon1", 2, 0, 0)
 
#Configure the Modbus driver
#drvModbusAsynConfigure(portName, tcpPortName, slaveAddress, modbusFunction,
#                       modbusStartAddress, modbusLength, dataType,
#                       pollMsec, plcType);
#In this example we poll a single bit at register address 1 every 1000ms.
drvModbusAsynConfigure("examplereadcoils", "tcpcon1", 0, 1, 1, 1, 0, 1000, "")
#drvModbusAsynConfigure("examplereadcoils", "tcpcon1", 0, 1, 1, 1, 0, 1000, "")

drvModbusAsynConfigure("examplewritecoils", "tcpcon1", 0, 5, 1, 1, 0, 5000, "")
 
#Load the database defining your EPICS records
dbLoadRecords(test.db)
