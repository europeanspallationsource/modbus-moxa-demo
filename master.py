#!/usr/bin/python

import sys
from pymodbus.client.sync import ModbusTcpClient
from pymodbus.transaction import ModbusRtuFramer, ModbusAsciiFramer

client = ModbusTcpClient('10.4.3.90', port=4015, framer=ModbusRtuFramer)
#client = ModbusTcpClient('10.4.3.90', port=4015, framer=ModbusAsciiFramer)
rq = client.write_coil(1, int(sys.argv[1]))
result = client.read_coils(1,1)
print result.bits[0]
client.close()
